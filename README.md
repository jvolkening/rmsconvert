# rmsconvert

This package consists of two helper programs for utilizing the ProteoWizard `msconvert` software over a network.
The intended use case is for running `msconvert` jobs on a Windows host from a \*nix platform.
The `rmsconvertd` daemon runs on the Windows system and listens from incoming connections from `rmsconvert` clients.
The client and server perform bidirectional TLS authentication and then the client transfers the input file along with the command parameters.
The server validates the file and parameters, runs `msconvert`, and returns the output data to the client.

These programs are currently undocumented. Use at your own risk.

## WARNING

The TLS certificates and keys that are included in this repository are for **testing purposes only!**.
If you are using the client/server in production, generate your own sets of authentication credentials for each server and client.
Otherwise, anyone will be able to connect to your server and perform **unauthorized mass spectrometry file conversion!!!** (or possibly exploit bugs in the software).

