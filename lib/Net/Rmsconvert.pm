package Net::Rmsconvert;

use strict;
use warnings;
use 5.012;

use Digest::MD5;
use File::Basename qw/basename/;
use File::Copy qw/copy/;
use File::Temp;
use FindBin;
use IO::Compress::Bzip2 qw/bzip2 $Bzip2Error/;
use IO::Compress::Gzip qw/gzip $GzipError/;
use IO::Socket::SSL;
use IO::Uncompress::Bunzip2 qw/bunzip2 $Bunzip2Error/;
use IO::Uncompress::Gunzip qw/gunzip $GunzipError/;
use List::Util qw/min/;

use constant MAX_LEN_FN => 255;

our $VERSION = 0.001;

sub new {

    my ($class, %args) = @_;

    $args{compression} //= 'none';
    $args{port}        //= '22223';
    $args{timeout}     //= '1200';

    # check for defined args
    for (qw/server params ssl_key ssl_crt ssl_ca compression port timeout/) {
        die "Param $_ must be defined"
            if (! defined $args{$_});
    }

    # check for valid filenames
    for (qw/ssl_key ssl_crt ssl_ca/) {
        die "Param $_ must be a valid file"
            if (! -r $args{$_});
    }

    # check for valid types
    for (qw/params/) {
        die "Param $_ must be an array ref"
            if (ref($args{$_}) ne 'ARRAY');
    }

    my $self = bless {%args} => $class;

    return $self;

}

sub convert {

    my ($self, $fn_in, $fn_out) = @_;

    my $session = IO::Socket::SSL->new(
        PeerAddr          => $self->{server},
        PeerPort          => $self->{port},
        Proto             => 'tcp',
        SSL_ca_file       => $self->{ssl_ca},
        SSL_cert_file     => $self->{ssl_crt},
        SSL_key_file      => $self->{ssl_key},
        SSL_use_cert      => 1,
        SSL_verify_mode   => SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT,
        SSL_verifycn_name => '2',
    ) or die "error opening socket: $@";

    $self->{session} = $session;

    $self->shake_hands() or die "Error during handshake\n";

    $self->send_params() or die "Error sending params\n";

    $self->send_file($fn_in) or die "Error during file transfer\n";

    if ($self->_read_nb(2) ne 'OK') {
        warn "Conversion failed\n";
        return 0;
    }

    $self->receive_file($fn_out) or die "Error during file retrieval\n";

    $self->{session} = undef;

    return 1;

}

sub _write {

    my ($self, $str) = @_;
    print {$self->{session}} $str;

}

sub shake_hands {

    my ($self) = @_;
    $self->_write('RMSC');
    return $self->_read_nb(4) eq 'CSMR';

}

sub send_params {

    my ($self) = @_;

    my @params = @{ $self->{params} };
    my $p_string = '';
    for my $p (@params) {
        my $l = length($p);
        die "maximum parameter length is 255\n" if ($l > 255);
        $p_string .= pack 'C', $l;
        $p_string .= $p;
    }
    $p_string .= pack 'C', 0;
    $self->_write($p_string);
    return $self->_read_nb(2,600) eq 'OK'
        ? 1
        : 0;

}

sub receive_file {

    my ($self, $fn_out) = @_;

    my $md5 = Digest::MD5->new();

    my $size = unpack 'N', $self->_read_nb(4) or return undef;

    my $tmp = File::Temp->new( UNLINK => 1 );

    my $remaining = $size;
    while ($remaining > 0) {
        my $bytes = min(32768,$remaining);
        my $chunk = $self->_read_nb($bytes);
        return undef if (! defined $chunk);
        print {$tmp} $chunk;
        $remaining -= length($chunk);
    };
    close $tmp;

    my $md5_given = $self->_read_nb(16);

    my $compression = $self->{compression};

    if ($compression eq 'bzip2') {
        my $tmp2 = File::Temp->new( UNLINK => 1 );
        my $status = bunzip2("$tmp" => $tmp2, 'BinModeOut' => 1)
            or die "bunzip2 failed: $Bunzip2Error\n";
        close $tmp2;
        $tmp = $tmp2;
    }
    elsif ($compression eq 'gzip') {
        my $tmp2 = File::Temp->new( UNLINK => 1 );
        my $status = gunzip("$tmp" => $tmp2, 'BinModeOut' => 1)
            or die "gunzip failed: $GunzipError\n";
        close $tmp2;
        $tmp = $tmp2;
    }

    # verify uncompressed MD5
    open my $fh, '<:raw', "$tmp";
    $md5->addfile($fh);
    close $fh;
    if ($md5_given ne $md5->digest) {
        $self->_write('ER');
        return 0;
    }

    $self->_write('OK');
    copy( "$tmp" => $fn_out );
    return 1;

}

sub send_file {

    my ($self, $fn) = @_;

    # calculate uncompressed MD5 to send
    open my $fh, '<:raw', $fn;
    my $md5 = Digest::MD5->new();
    $md5->addfile($fh);
    close $fh;

    # send original filename
    my $base = basename($fn);
    my $l_fn = length $base;
    die "input filename too long"
        if ($l_fn > MAX_LEN_FN);
    $self->_write( pack('C',$l_fn) );
    $self->_write($base);

    my $compression = $self->{compression};

    if ($compression eq 'bzip2') {
        my $tmp = File::Temp->new( UNLINK => 1 );
        my $status = bzip2 $fn => $tmp, 'BinModeIn' => 1
            or die "bzip2 failed: $Bzip2Error\n";
        close $tmp;
        $fn = $tmp;
        $self->_write( pack('C',2) );
    }
    elsif ($compression eq 'gzip') {
        my $tmp = File::Temp->new( UNLINK => 1 );
        my $status = gzip $fn => $tmp, 'BinModeIn' => 1
            or die "gzip failed: $GzipError\n";
        close $tmp;
        $fn = $tmp;
        $self->_write( pack('C',1) );
    }
    elsif ($compression eq 'none') {
        $self->_write( pack('C',0) );
    }
    else {
        die "Unknown compression type: $compression"
    }

    open $fh, '<:raw', "$fn";

    my $file_size = -s "$fn";
    $self->_write( pack('N',$file_size) );

    my $buffer;
    my $remaining = $file_size;
    while ($remaining > 0) {
        my $bytes = min(32768,$remaining);
        my $r = read $fh, $buffer, $bytes;
        $self->_write( $buffer );
        $remaining -= $r;
    }
    close $fh;
    $self->_write( $md5->digest );

    return $self->_read_nb(2,600) eq 'OK'
        ? 1
        : 0;

}

sub _read_nb {

    my ($self, $len, $timeout) = @_;

    $timeout //= $self->{timeout};
    my $socket = $self->{session};

    my $buffer;
    eval {
        local $SIG{ALRM} = sub {};
        alarm($timeout);
        read $socket, $buffer, $len;
    };
    alarm(0);
    return $buffer;

}

1;
